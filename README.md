# OpenML dataset: Loan-Predication

https://www.openml.org/d/43595

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Among all industries, insurance domain has the largest use of analytics  data science methods. This data set would provide you enough taste of working on data sets from insurance companies, what challenges are faced, what strategies are used, which variables influence the outcome etc. This is a classification problem. The data has 615 rows and 13 columns.
Problem-----
Company wants to automate the loan eligibility process (real time) based on customer detail provided while filling online application form. These details are Gender, Marital Status, Education, Number of Dependents, Income, Loan Amount, Credit History and others. To automate this process, they have given a problem to identify the customers segments, those are eligible for loan amount so that they can specifically target these customers. Here they have provided a partial data set.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43595) of an [OpenML dataset](https://www.openml.org/d/43595). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43595/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43595/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43595/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

